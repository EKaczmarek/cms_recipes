"""CMS_proj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib import admin
from django.conf import settings # new
from django.urls import path, include # new
from django.conf.urls.static import static # new


from polls import views

urlpatterns = [
    # path('recipes/', include('recipes.urls')),    
    path('polls/', include('polls.urls')),    
    path('admin/', admin.site.urls),
    path('categories/', include('recipes.urls')),
    path('accounts/', include('django.contrib.auth.urls')), # new
    # path('', views.IndexView.as_view(), name="index"),
    path('recipes/', include('recipes.urls')),
    path('', include('recipes.urls')),
]

if settings.DEBUG:
        urlpatterns += static(settings.MEDIA_URL,
                              document_root=settings.MEDIA_ROOT)

