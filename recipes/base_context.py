

from recipes.models import Category

def base_html_context(request):
    return {
        'categories': Category.objects.all()
    }
