from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render, get_object_or_404, redirect
from django.views import generic
from django.urls import reverse_lazy
from itertools import chain
from .models import Category, Recipe
from recipes.forms import CreateRecipeForm
from django.contrib.auth.decorators import login_required
import logging

logger = logging.getLogger(__name__)

# @login_required
def index(request):
    search_query = request.GET.get('search-parameter')
    recipe_categories = request.GET.get('category')
    logger.info(recipe_categories)
    recipes = Recipe.objects.order_by('id')

    if recipe_categories:
        recipes = Recipe.objects.filter(r_category__icontains=recipe_categories)
    if search_query:
        recipes = Recipe.objects.filter(r_name__icontains=search_query)

    context = {
        'recipes_list': recipes
    }
    context['categories'] = Category.objects.all()  

    return render(request, 'recipes/index.html', context)


# @login_required
def details(request, recipe_id):
    recipe = get_object_or_404(Recipe, pk=recipe_id)

    context = {'recipe': recipe}
    return render(request, 'recipes/detail.html', context)

# @login_required
def edit(request, recipe_id = None):
    if recipe_id:
        instance = Recipe.objects.get(pk=recipe_id)
    else:
        instance = None
    
    if request.method == 'POST':
        form = CreateRecipeForm(request.POST, request.FILES, instance=instance)

        if form.is_valid():
            recipe = form.save(commit=True)
            recipe.save()            

               
            return redirect('/recipes/')
    else:
        form = CreateRecipeForm(instance=instance)
    return render(request, 'recipes/edit.html', {'form': form})


# @login_required
def delete(request, recipe_id):   
    recipe = get_object_or_404(Recipe, pk=recipe_id)
    if request.method == 'GET':
        context = {'recipe': recipe}
        return render(request, 'recipes/delete.html', context)
    else:
        recipe_id = int(recipe_id)
        try:
            recipe_to_del = Recipe.objects.get(id = recipe_id)
        except Recipe.DoesNotExist:
            return redirect('/')

        recipe_to_del.delete()
        return redirect('/')



