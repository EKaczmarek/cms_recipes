
from django.contrib import admin 
from django.urls import path


from . import views

# app_name = 'recipes'
urlpatterns = [
    path('', views.index, name='index_recipes'),
    path('recipes/', views.index, name='index_recipes'),
    path('new/', views.edit, name='create_recipe'),
    path('recipe/<int:recipe_id>/', views.details, name='detail_recipe'),
    path('recipe/<int:recipe_id>/edit', views.edit, name='edit_recipe'),
    path('recipe/<int:recipe_id>/delete', views.delete, name='delete_recipe'),
    path('recipe/search', views.index, name='search_results'),
]   
    