from django import forms
from recipes.models import Recipe, Category, Ingredient
from django.utils.translation import gettext_lazy as _
from django.forms import formset_factory


class CreateRecipeForm(forms.ModelForm):
    
    class Meta:
        model = Recipe
        fields = ('r_name', 'r_picture', 'r_cook_time', 'r_prepare_time', 'r_category', 'r_opis')
        error_messages = {
            'r_name': {
                'required' : _("Nazwa potrawy jest wymagana!")
            },
            'r_picture' : {
                'required' : _("Zdjęcie jest wymagane!")
            },
            'r_cook_time': {
                'required' : _("Czas gotowania jest wymagany!")
            },
            'r_prepare_time' : {
                'required' : _("Czas przygotowania jest wymagany!")
            },
            'r_category': {
                'required': _('Kategoria jest wymagana!')
            },            
            'r_opis': {
                'required': _('Opis przygotowania jest wymagany!')
            }
        }
 