# Generated by Django 3.0.1 on 2020-01-06 14:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='recipe',
            name='r_ingredients',
            field=models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='recipes.Ingredient', verbose_name='Składniki'),
        ),
    ]
