import datetime

from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from datetime import datetime

class Ingredient(models.Model):
    i_name = models.CharField('Nazwa składnika', max_length=50)

    def __str__(self):
        return self.i_name

class Category(models.Model):
    c_name = models.CharField('Nazwa kategorii', max_length=50, default='obiad')

    def __str__(self):
        return self.c_name
    
    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"

class Recipe(models.Model):
    r_name = models.CharField('Nazwa potrawy', max_length=100)
    r_prepare_time = models.IntegerField('Czas przygotowania')
    r_cook_time = models.IntegerField('Czas gotowania')
    r_picture = models.ImageField('Zdjęcie', upload_to='images/')
    r_opis = models.TextField('Opis przygotowania', max_length=1000, default='')
    r_category = models.ForeignKey('Category', on_delete=models.DO_NOTHING, default=1, verbose_name='Kategoria')

    def __str__(self):
        return self.r_name
    
