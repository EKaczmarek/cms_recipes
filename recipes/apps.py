from django.apps import AppConfig


class RecipesConfig(AppConfig):
    name = 'recipes'

class CategoryConfig(AppConfig):
    name = "Category"
    verbose_name = "Categories"
