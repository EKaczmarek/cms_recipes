from django.contrib import admin

from .models import Recipe, Category, Ingredient

from polls.models import Question


class CategoryInline(admin.StackedInline):
    model = Category
    extra = 1


class IngredientsInline(admin.StackedInline):
    model = Category
    extra = 1

class RecipeAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,                              {'fields': ['r_name', 'r_picture', 'r_cook_time']}),
        ('Steps information',               {'fields': ['r_prepare_time'], 'classes': ['collapse']}),
    ]


admin.site.register(Recipe, RecipeAdmin)


admin.site.register(Category)
admin.site.register(Ingredient)
